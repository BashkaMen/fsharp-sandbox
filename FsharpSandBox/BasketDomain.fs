﻿namespace FsharpSandBox.BasketDomain

open System

type CardNumber = private CardNumber of string
type Cvv = private Cvv of int

type CardPayment = { Number: CardNumber; ExpireDate: DateTime; Cvv: Cvv }
type CheckPayment = { Number: string; }

type PaymentMethod =
    | Card of CardPayment
    | Check of CheckPayment
    
type Payment = {
    Method: PaymentMethod
    Amount: int
}

type BasketLine = { Name: string; Amount: int }

type ActiveBasket = { Id: Guid; UnpaidItems: BasketLine list }
type PaidBasket = { Id: Guid; PaidItems: BasketLine list; Payment: Payment }

type Basket =
    | Active of ActiveBasket
    | Paid of PaidBasket
 

module Basket =
    let ( ^ ) f x = f x
    
    let createCardNumber (number: string) =
        match number with
        | x when x.Length = 16 -> Ok ^ CardNumber x
        | _ -> Error "Invalid card number"
        
    let createCvv (number: int) =
        match number with
        | x when x >= 100 && x <= 999 -> Ok x
        | _ -> Error "Invalid cvv"
    
    let add item basket =
        let items = item :: basket.UnpaidItems
        { basket with UnpaidItems = items }

    
    let remove item basket =
        basket.UnpaidItems
        |> List.filter ^ (=) item
        |> fun items -> { basket with UnpaidItems = items }
        
    
    let calcAmount basket =
        basket.UnpaidItems
        |> Seq.sumBy ^ fun x -> x.Amount
        
        
    let pay payment basket =
        calcAmount basket
        |> fun amount -> { Method = payment; Amount = amount }
        |> fun payment -> { Id = basket.Id; PaidItems = basket.UnpaidItems; Payment = payment }
    
    
    type AddItemError =
        | BasketArePaid
        
    let tryAdd item = function
        | Active x -> add item x |> Ok
        | Paid x -> Error ^ BasketArePaid
        
    
    type PayBasketError =
        | BasketAlreadyPaid
        | PaymentMethodNotSupported
    
    let tryPay payment = function
        | Active x ->
            match payment with
            | Check check -> pay payment x |> Ok
            | Card card -> Error ^ PaymentMethodNotSupported 
        | Paid x -> Error ^ BasketAlreadyPaid