﻿namespace FsharpSandBox.Infrastructure

    open System
    open FsharpSandBox.BasketDomain

    type IRepository =
        abstract member Save : Basket -> Async<unit>
        abstract member Get : Guid -> Async<Basket>



namespace FsharpSandBox.BasketApp

open System
open System.Diagnostics
open FsToolkit.ErrorHandling
open FsharpSandBox.BasketDomain
open FsharpSandBox.Infrastructure


type CreateCommand = class end
type AddLineCommand = { BasketId:Guid; Line:BasketLine }
type PayCommand = { BasketId: Guid; Payment: PaymentMethod }
type GetAmountQuery = { BasketId: Guid; }


module BasketApp =
    
    let create (repos: IRepository) (cmd: CreateCommand) = async {
        let tracer = obj()
        let basket = { Id = Guid.NewGuid(); UnpaidItems = [] }
        do! repos.Save(Active basket)
        return basket.Id
    }
        
    let addLine (repos:IRepository) (cmd: AddLineCommand) = asyncResult {
        let! basket = repos.Get(cmd.BasketId)
        let! basket = Basket.tryAdd cmd.Line basket
        do! repos.Save(Active basket)
    }
    
    let pay (repos: IRepository) (cmd: PayCommand) = asyncResult {
        let! basket = repos.Get(cmd.BasketId)
        let! paidBasket = Basket.tryPay cmd.Payment basket
        do! repos.Save(Paid paidBasket)
    }
    
    
        
        
namespace FsharpSandBox.Main

open System
open FsharpSandBox.BasketDomain
open FsharpSandBox.BasketApp
open FsharpSandBox.Infrastructure

    
    
module Dependency =
    
    open FsToolkit.ErrorHandling
    
    type IBasketDispatcher =
        abstract member create: CreateCommand -> Async<Guid>
        abstract member addLine: AddLineCommand -> Async<Result<unit, Basket.AddItemError>>
        abstract member pay: PayCommand -> Async<Result<unit, Basket.PayBasketError>>
        
    type BasketDispatcher2 = {
        Create: CreateCommand -> Async<Guid>
        AddLine: AddLineCommand -> Async<Result<unit, Basket.AddItemError>>
        Pay: PayCommand -> Async<Result<unit, Basket.PayBasketError>>
    }
    
    let repos : IRepository = failwith "??"
    let basketDispatcher =
        { new IBasketDispatcher with
            member z.create cmd = BasketApp.create repos cmd
            member z.addLine cmd = BasketApp.addLine repos cmd
            member z.pay cmd = BasketApp.pay repos cmd
        }
    
    let basketDispatcher2 = {
        Create = BasketApp.create repos
        AddLine = BasketApp.addLine repos
        Pay = BasketApp.pay repos
    }
    
    let run() = asyncResult {
        let cmd = { BasketId = Guid.NewGuid(); Payment = Check { Number = "123123245356487897543" } }
        do! basketDispatcher.pay(cmd)
        do! basketDispatcher2.Pay(cmd)
    }